const { test, expect } = require('@playwright/test');
const { LuluProductPage } = require('../pom/luluProductPage');
const { LuluSearchPage } = require('../pom/luluSearchPage');

test.beforeEach(async ({ page }) => {
    await page.goto('https://www.lulu.com/search?page=1&q=&pageSize=10&adult_audience_rating=00');
  });

test.describe('Try to search for a product in the bookstore', () => {
    test('[BKS-03001] Search for any product in bookstore', async ({ page }) => {
        const luluSearchPage = new LuluSearchPage(page);
        await luluSearchPage.isVisible();
        await luluSearchPage.searchBarIsVisible();
        await luluSearchPage.fillSearchBarAndTapEnter('Think and Grow Rich');
        await luluSearchPage.firstProductTitleIsCorrect('Think and Grow Rich');
    });

    test('[BKS-03002] Search for the product the bookstore for any non-default language', async ({ page }) => {
        const luluSearchPage = new LuluSearchPage(page);
        const luluProductPage = new LuluProductPage(page);
        await luluSearchPage.isVisible();
        await luluSearchPage.filterLanguageIsVisible();
        await luluSearchPage.clickSpanishLanguageOnTheFilter();
        await luluSearchPage.sortByDropDownIsVisible();
        await luluSearchPage.selectOptionFromSortByDropDown();
        await luluSearchPage.clickOnTheFirstProduct();
        await luluProductPage.isVisible();
        await luluProductPage.productPriceIsCorrect('0.00');
        await luluProductPage.productLanguageIsCorrect('Spanish');
    });
    
    test('[BKS-03003] Search for any non-existing product in the bookstore', async ({ page }) => {
        const luluSearchPage = new LuluSearchPage(page);
        await luluSearchPage.isVisible();
        await luluSearchPage.searchBarIsVisible();
        await luluSearchPage.fillSearchBarAndTapEnter('qwert12345^&*(');
        await luluSearchPage.resultInformationHasExpectedText('No search results');
    });
    
    test('[BKS-03004] Display bookstore categories while searching from the homepage', async ({ page }) => {
        const luluSearchPage = new LuluSearchPage(page);
        await luluSearchPage.isVisible();
        await luluSearchPage.bookstorCategoriesToggleIsVisible();
        await luluSearchPage.bookstorCategoriesToggleHasProperlyText('Show Bookstore Categories');
        await luluSearchPage.clickOnTheBookstoreCategoriesToggle();
        await luluSearchPage.bookstorCategoriesListIsVisible();
        await luluSearchPage.bookstorCategoriesToggleHasProperlyText('Hide Bookstore Categories');
    });
});
