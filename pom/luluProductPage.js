const { expect } = require('@playwright/test');

exports.LuluProductPage = class LuluProductPage {
    /**
    * @param {import('@playwright/test').Page} page
     */

    constructor(page) {
        this.page = page;
        this.productPage = page.locator('[class*="IntroSection_productPage__intro__"]');
        this.productPrice = page.locator('[class*="bookFormatPriceCurrent"]');
        this.productDetailsList = page.locator('[class*="ProductDescriptionSection_specList"] > dd');
    }

    async isVisible() {
        await expect(this.productPage.nth(0)).toBeVisible();
    }

    async productPriceIsCorrect(price) {
        const priceWithCurrency = await this.productPrice.textContent();
        const currency = priceWithCurrency.substring(0,3);
        if(currency == 'USD')
            await expect(this.productPrice).toHaveText('USD ' + price);
        else if(currency == 'EUR') {
            await expect(this.productPrice).toHaveText('EUR ' + price);
        }
    }

    async productLanguageIsCorrect(language) {
        await expect(this.productDetailsList.nth(1)).toHaveText(language);
    }
};
