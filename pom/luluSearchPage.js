const { expect } = require('@playwright/test');

exports.LuluSearchPage = class LuluSearchPage {
    /**
    * @param {import('@playwright/test').Page} page
     */

    constructor(page) {
        this.page = page;
        this.luluSearchPage = page.locator('.full-height-container');
        this.searchBar = page.getByTestId('header-search-input');
        this.productTitle = page.getByTestId('product-title');
        this.filterLanguage = page.getByTestId('search-facet-language');
        this.filterSpanishLanguage = page.getByTestId('search-facet-spa');
        this.sortByDropDown = page.locator('[class="SortBar_sortBar__dropdown__ZmgHc"]');
        this.sortByOptions = page.locator('[class="SortBar_menuItem__dropdownMenu__9eH7h"] > a');
        this.resultInfo = page.locator('[class="centered-text subhead"]');
        this.bookstoreCategoriesToggle = page.getByTestId('header-toggle-categories');
        this.bookstoreCategoriesToggleText = page.locator('[data-testid="header-toggle-categories"] > p');
        this.bookstoreCategoriesList = page.locator('[class*="Search_categoryDropdownWrapper"]');
    }

    async isVisible() {
        await expect(this.luluSearchPage).toBeVisible();
    }

    async searchBarIsVisible() {
        await expect(this.searchBar).toBeVisible();
    }

    async fillSearchBarAndTapEnter(title) {
        await this.searchBar.fill(title);
        await this.searchBar.press('Enter');
    }

    async firstProductTitleIsCorrect(title) {
        await expect(this.productTitle.nth(0)).toHaveText(title);
    }

    async filterLanguageIsVisible() {
        await expect(this.filterLanguage).toBeVisible();
    }

    async clickSpanishLanguageOnTheFilter() {
        await this.filterSpanishLanguage.click();
    }

    async sortByDropDownIsVisible() {
        await expect(this.sortByDropDown).toBeVisible();
    }

    async selectOptionFromSortByDropDown() {
        await this.sortByDropDown.hover();
        await this.page.waitForTimeout(2000);
        await this.sortByOptions.nth(3).hover();
        await this.sortByOptions.nth(3).click();
        await this.page.waitForTimeout(2000);
    }

    async clickOnTheFirstProduct() {
        await this.productTitle.nth(0).click();
    }

    async resultInformationHasExpectedText(text) {
        await expect(this.resultInfo).toHaveText(text);
    }

    async bookstorCategoriesToggleIsVisible() {
        await expect(this.bookstoreCategoriesToggle).toBeVisible();
    }

    async clickOnTheBookstoreCategoriesToggle() {
        await this.bookstoreCategoriesToggle.click();
    }

    async bookstorCategoriesListIsVisible() {
        await expect(this.bookstoreCategoriesList).toBeVisible();
    }

    async bookstorCategoriesToggleHasProperlyText(text) {
        await expect(this.bookstoreCategoriesToggleText).toHaveText(text);
    }
};
